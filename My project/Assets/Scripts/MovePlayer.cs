using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed = .1f;

    // Update is called once per frame
    void Update()
    {
        // vector 3 is a variable that takes in 3 inputs, x y and z
        Vector3 move = (Input.GetAxis("Horizontal") * Vector3.right) + (Input.GetAxis("Vertical")Vector3.forward);
        move = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up) move;
        transform.position += move.normalized * speed * Time.deltaTime;
    }
}
