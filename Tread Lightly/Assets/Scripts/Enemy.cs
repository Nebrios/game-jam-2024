using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    PathNode target, imhere;
    [SerializeField] GameObject player;
    [SerializeField] float speed;
    int progress; public float sleep = 100;
    public float detectDistance = 4;
    [SerializeField] float wakingSpeed;
    [SerializeField] Graph pathing;
    List<PathNode> path;
    bool reachedTarget = true;
    // Start is called before the first frame update
    public void StartGame()
    {
        sleep = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.SqrMagnitude(player.transform.position-transform.position)<detectDistance*detectDistance)
            sleep -= Time.deltaTime * wakingSpeed;
        if (Vector3.SqrMagnitude(player.transform.position - transform.position) < (detectDistance) * (detectDistance/2))
            sleep -= Time.deltaTime * wakingSpeed;
        if (Vector3.SqrMagnitude(player.transform.position - transform.position) < (detectDistance/2) * (detectDistance / 2))
            sleep -= Time.deltaTime * wakingSpeed;
        foreach (Graph.Node n in pathing.nodes)
        {
            if (Vector3.SqrMagnitude(n.pathNode.pos - transform.position) < 0.1)
                imhere = n.pathNode;
        }
        if (imhere == target) reachedTarget = true;
        if (reachedTarget)
        {
            Debug.Log(pathing.nodes.Count);
            target = pathing.nodes[Mathf.FloorToInt(Random.Range(0, pathing.nodes.Count))].pathNode;
            reachedTarget = false;
            progress = 0;
            path = pathing.BreadthFirstSearch(imhere, target);
        }
        transform.position = Vector3.MoveTowards(transform.position, path[progress].pos, speed*Time.deltaTime);
        if (imhere == path[progress]) progress++;
        transform.LookAt(path[progress].transform);
    }
    public void SetDestination(Vector3 location)
    {
        PathNode p;
        foreach (Graph.Node n in pathing.nodes)
        {
            if (Vector3.SqrMagnitude(location - n.pathNode.pos) < 1)
            {
                p = n.pathNode; break;
            }
        }
        path = pathing.BreadthFirstSearch(imhere, target);
        reachedTarget = false;
        progress = 0;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "PlayerAura")
        {
            sleep -= Time.deltaTime * wakingSpeed;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerAura")
        {
            sleep -= Time.deltaTime * wakingSpeed;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerAura")
        {
            sleep -= Time.deltaTime * wakingSpeed;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerAura")
        {
            sleep -= Time.deltaTime * wakingSpeed;
        }
    }
}
