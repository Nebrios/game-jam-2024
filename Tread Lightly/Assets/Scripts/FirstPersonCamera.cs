using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstPersonCamera : MonoBehaviour
{
    public PlayerMove player;
    public float mouseSensitivity = 2f;
    float cameraVerticalRotation = 0f;
    [SerializeField] Light lantern;
    public float oil = 100;
    [SerializeField] float oilCD = 2;
    float oilTImer = 2;
    [SerializeField] TMPro.TextMeshProUGUI oilText;
    [SerializeField] Enemy enemy;
    bool lockedCursor = true;
    [SerializeField] bool dimmed = false;
    // Start is called before the first frame update
    void Start()
    {
        // lock and hide the cursor during play
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {
        // collect mouse input
        float inputX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float inputY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        // rotate the camera around its local x axis
        cameraVerticalRotation -= inputY;
        cameraVerticalRotation = Mathf.Clamp(cameraVerticalRotation, -90f, 90f);
        transform.localEulerAngles = Vector3.right * cameraVerticalRotation;

        // rotate the Player Object and the Camera around its y axis
        player.transform.Rotate(Vector3.up * inputX);
        if (Input.GetMouseButtonDown(0))
        {
            // make line in from camera
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 4f))
            {
                Item item = hit.collider.gameObject.GetComponent<Item>();
                if (item != null)
                {
                    item.PickUp(player.inventory);
                }

            }
        }
        if (Input.GetKeyDown(KeyCode.Q) && oil>0)
        {
            dimmed = !dimmed;
            if (dimmed)
            {
                lantern.intensity = 3;
                RenderSettings.fogDensity = 0.4f;
                enemy.detectDistance = 2.5f;
            }
            else
            {
                lantern.intensity = 12;
                RenderSettings.fogDensity = 0.01f;
                enemy.detectDistance = 5;
            }
        }
        if (oil <= 0)
        {
            lantern.intensity = 0;
            RenderSettings.fogDensity = 0.5f;
            enemy.detectDistance = 5;
        }
        else {
            oilTImer -= Time.deltaTime;
            if (oilTImer <= 0)
            { oil--;
                oilTImer = oilCD;
            }
        }
        oilText.text = "Oil remaining: " + oil + "%";
    }
}
