using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    List<PathNode> pathBreadth = new List<PathNode>();
    List<PathNode> pathHeuristic = new List<PathNode>();
    List<PathNode> pathDijikstras = new List<PathNode>();
    List<PathNode> pathAStar = new List<PathNode>();
    // This class represents a node in our graph
    public class Node
    {
        public PathNode pathNode;
        public List<Node> connectedNodes = new List<Node>();
    }
    public List<Node> nodes = new List<Node>();
    public Node youAreHere = null;
    void Awake()
    {
    }
    // Add a new node to the graph
    public void AddNode(PathNode aPathNode)
    {
        Node n = new Node();
        n.pathNode = aPathNode;
        nodes.Add(n);
        n.connectedNodes.Clear();
        n.pathNode.connections.Clear();
        for(int i = 0; i<nodes.Count; i++)
        {
            if (nodes[i]!=null)
            { Node aNode = nodes[i];
                if (Vector3.SqrMagnitude(n.pathNode.pos - aNode.pathNode.pos) < 2.75 && aNode.pathNode.gameObject.name!=n.pathNode.gameObject.name)
                {
                    if (!n.connectedNodes.Contains(aNode)) { n.connectedNodes.Add(nodes[i]); n.pathNode.connections.Add(nodes[i].pathNode.gameObject.name); }
                    if (!aNode.connectedNodes.Contains(n)) { nodes[i].connectedNodes.Add(n); nodes[i].pathNode.connections.Add(n.pathNode.gameObject.name); }
                }
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PathNode startN = nodes[Random.Range(0, nodes.Count)].pathNode;
            PathNode endN = nodes[Random.Range(0, nodes.Count)].pathNode;
            pathBreadth = BreadthFirstSearch(startN, endN);

            pathHeuristic = HeuristicSearch(startN, endN);

        }
        if(Input.GetKeyDown(KeyCode.Comma))
        {
        //pathDijikstras=DijkstrasSearch(nodes[Random.Range(0, nodes.Count)].pathNode, nodes[Random.Range(0, nodes.Count)].pathNode);
        //pathAStar=AStarSearch(nodes[Random.Range(0, nodes.Count)].pathNode, nodes[Random.Range(0, nodes.Count)].pathNode);
        }
    }
    private void OnDrawGizmos()
    {
        if(nodes!=null)
        if(nodes.Count>0)
        for (int i = 0; i < nodes.Count; i++)
        {
                    if (nodes[i] != null)
                    {
                        Gizmos.color = Color.gray;
                        Gizmos.DrawSphere(nodes[i].pathNode.gameObject.transform.position, 0.2f);
                        Gizmos.color = Color.yellow;
                        for (int j = 0; j < nodes[i].connectedNodes.Count; j++)
                            Gizmos.DrawLine(nodes[i].pathNode.gameObject.transform.position, nodes[i].connectedNodes[j].pathNode.gameObject.transform.position);
                    } 
        }
        if (pathBreadth.Count > 0)
            for (int i = 0; i < pathBreadth.Count; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(pathBreadth[i].pos, 0.3f);
            }
        if (pathHeuristic.Count > 0)
            for (int i = 0; i < pathHeuristic.Count; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(pathHeuristic[i].pos, 0.3f);
            }
        if (pathDijikstras.Count > 0)
            for (int i = 0; i < pathDijikstras.Count; i++)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(pathDijikstras[i].pos, 0.3f);
            }
        if (pathAStar.Count > 0)
            for (int i = 0; i < pathAStar.Count; i++)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(pathAStar[i].pos, 0.3f);
            }
    }
    public List<PathNode> BreadthFirstSearch(PathNode start, PathNode end) {
        //Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        LinkedList<PathNode> frontier = new LinkedList<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        frontier.AddFirst(start);
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            foreach (Node n in nodes)
            {
                if(frontier.Count > 0)
                if (n.pathNode!=null && n.pathNode == frontier.First.Value)
                {
                    youAreHere = n; frontier.RemoveFirst();
                        if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                        foreach (Node nod in youAreHere.connectedNodes)
                        {
                            if (!visited.Contains(nod.pathNode))
                            {
                                nod.pathNode.parentNode = youAreHere.pathNode;
                                frontier.AddLast(nod.pathNode);
                                visited.Add(nod.pathNode);
                                if (nod.pathNode == end)
                                {
                                    frontier.Clear();
                                    endNode = nod.pathNode;
                                    break;
                                }
                            }
                        }
                    }
            }
        }
        if(endNode!=null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        //string printout="In " + visited.Count + " steps: ";
        //foreach(PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        //Debug.Log(printout);
        return finalPath;
    }

    public List<PathNode> HeuristicSearch(PathNode start, PathNode end)
    {
        Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        LinkedList<PathNode> frontier = new LinkedList<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        float distance = Vector3.SqrMagnitude(start.pos- end.pos);
        frontier.AddFirst(start);
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            foreach (Node n in nodes)
            {
                if (frontier.Count > 0)
                    if (n.pathNode != null && n.pathNode == frontier.First.Value)
                    {
                        youAreHere = n; frontier.RemoveFirst();
                        if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                        foreach (Node nod in youAreHere.connectedNodes)
                        {
                            if (!visited.Contains(nod.pathNode))
                            {
                                nod.pathNode.parentNode = youAreHere.pathNode;
                                if (Vector3.SqrMagnitude(nod.pathNode.pos - end.pos) < distance)
                                {
                                    frontier.AddFirst(nod.pathNode); distance = Vector3.SqrMagnitude(nod.pathNode.pos - end.pos);
                                }
                                frontier.AddLast(nod.pathNode);
                                visited.Add(nod.pathNode);
                                if (nod.pathNode == end)
                                {
                                    frontier.Clear();
                                    endNode = nod.pathNode;
                                    break;
                                }
                            }
                        }
                    }
            }
        }
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        string printout = "In " + visited.Count + " steps: ";
        foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }

    public List<PathNode> DijkstrasSearch(PathNode start, PathNode end)
    {
        Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        LinkedList<PathNode> frontier = new LinkedList<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        frontier.AddFirst(start);
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            foreach (Node n in nodes)
            {
                if (frontier.Count > 0)
                    if (n.pathNode != null && n.pathNode == frontier.First.Value)
                    {
                        youAreHere = n; frontier.RemoveFirst();
                        if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                        foreach (Node nod in youAreHere.connectedNodes)
                        {
                            if (!visited.Contains(nod.pathNode))
                            {
                                nod.pathNode.parentNode = youAreHere.pathNode;
                                frontier.AddLast(nod.pathNode);
                                visited.Add(nod.pathNode);
                                if (nod.pathNode == end)
                                {
                                    frontier.Clear();
                                    endNode = nod.pathNode;
                                    break;
                                }
                            }
                        }
                    }
            }
        }
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        string printout = "";
        foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
    public List<PathNode> AStarSearch(PathNode start, PathNode end)
    {
        Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        LinkedList<PathNode> frontier = new LinkedList<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        frontier.AddFirst(start);
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            foreach (Node n in nodes)
            {
                if (frontier.Count > 0)
                    if (n.pathNode != null && n.pathNode == frontier.First.Value)
                    {
                        youAreHere = n; frontier.RemoveFirst();
                        if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                        foreach (Node nod in youAreHere.connectedNodes)
                        {
                            if (!visited.Contains(nod.pathNode))
                            {
                                nod.pathNode.parentNode = youAreHere.pathNode;
                                frontier.AddLast(nod.pathNode);
                                visited.Add(nod.pathNode);
                                if (nod.pathNode == end)
                                {
                                    frontier.Clear();
                                    endNode = nod.pathNode;
                                    break;
                                }
                            }
                        }
                    }
            }
        }
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        string printout = "";
        foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
}
