using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] Enemy enemy;
    [SerializeField] Graph graph;
    PathNode closestNode;
    private void Awake()
    {
        foreach (Graph.Node n in graph.nodes)
        {
            if (Vector3.SqrMagnitude(n.pathNode.pos - transform.position) < 4)
            { closestNode = n.pathNode; break; }
        }
    }
    public void PickUp(PlayerInventory playerInventory)
    {
        playerInventory.ItemCollected();
        gameObject.SetActive(false);
        if (closestNode != null)
        { enemy.SetDestination(closestNode.pos); }
    }
}
