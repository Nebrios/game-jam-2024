using System.Collections;  
using System.Collections.Generic;  
using UnityEngine;  
using UnityEngine.SceneManagement;  

public class MainMenu: MonoBehaviour {  
    public void PlayGame() {  
        SceneManager.LoadScene("Test Level");  
    }  

    public void QuitGame() {  
        Debug.Log("QUIT");  
        Application.Quit();  
    }  
}   