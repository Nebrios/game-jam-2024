using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    [SerializeField] private Graph theGraph;
    public List<string> connections;
    public Vector3 pos;
    public float weight;
    public PathNode parentNode;
    //bool awakened = false;
    
    // Start is called before the first frame update
    void Awake()
    {
        pos = this.gameObject.transform.position;
        theGraph.AddNode(this);
    }
    private void OnDrawGizmos()
    {
        //if (!awakened) { Awake(); awakened = true; }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
