using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] bool isInMenu;
    public PlayerInventory inventory;
    GameObject shopMenu;
    // Start is called before the first frame update
    void Start()
    {
        isInMenu = false;
        shopMenu = GameObject.Find("ShopMenuPrefab");
    }

    // Update is called once per frame
    void Update()
    {
        if (!isInMenu)
        {
            Vector3 move = (Input.GetAxisRaw("Horizontal") * Vector3.right) + (Input.GetAxisRaw("Vertical") * Vector3.forward);
            move = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up) * move;
            transform.position += move.normalized * speed * Time.deltaTime;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Fab Exit_Door")
        {
            isInMenu = true;
            shopMenu.SetActive(true);
        }
    }
}
