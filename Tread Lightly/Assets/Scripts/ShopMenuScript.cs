using System.Collections;  
using System.Collections.Generic;  
using UnityEngine;  
using UnityEngine.SceneManagement;  

public class ShopMenuScript : MonoBehaviour
{
    public void ReloadGame() {  
        SceneManager.LoadScene("Test Level");  
    }  

    public void QuitGame() {  
        Debug.Log("QUIT");  
        Application.Quit();  
    }  
}
